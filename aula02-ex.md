# Exercícios da aula 2

**1.** Nas linhas abaixo, indique a palavra que o shell tentará interpretar como um comando?

```
:~$ banana laranja
```

Resposta:


```
:~$ > elefante zebra tigre
```

Resposta:

```
:~$ ciano=amarelo verde azul > roxo
```

Resposta:

```
1elefante='4 cavalos' 30 cachorros
```

Resposta:

---

**2.** Analise a lista, abaixo, e responda o que for pedido:

- `cat`
- `cd`
- `mkdir`
- `env`
- `rm`
- `mv`
- `ls`
- `cp`

**a.** Na lista, quem são os comandos internos do Bash?

**b.** Como obter ajuda para o comando `cd`?

**c.** Na lista, o que você utilizaria para renomear um arquivo?

**d.** Em um comando simples, quais nomes da lista seriam executados em um novo processo?

**e.** Qual é a opção do `mkdir` para criar, com apenas um comando, um diretório e, nele, um subdiretório?

---

**3.** Comente os comandos, abaixo, para obter os resultados solicitados:

Definir a variável `var` na sessão corrente do shell:

```
:~$ var=banana ls -l > arquivos.txt
```

Imprimir a saída do comando no terminal:

```
:~$ cat /etc/shells > shells.txt
```

Criar o arquivo vazio `novo.txt`:

```
:~$ > novo.txt ls
```

---

**4.** Como você executaria o script `nome-exp.sh` para obter ***Salve, Luis Carlos!*** na saída?

Script:

```
#!/bin/bash

echo "Salve, $nome"
```

Resposta:

```
:~$ 
```

---

**5.** Como você executaria o script `nome-arg.sh` para obter ***Salve, Luis Carlos!*** na saída?

Script:

```
#!/bin/bash

echo "Salve, $1"
```

Resposta:

```
:~$ 
```

---

**6.** Observe a invocação do script abaixo e responda:

```
:~$ ./script.sh banana laranja abacate
```

**a.** Qual seria o valor expandido pelo parâmetro `#`?

**b.** Qual seria o valor expandido pelo parâmetro posicional `1`?

**c.** Qual seria o valor expandido pelo parâmetro posicional `4`?

**d.** Qual seria o valor expandido pelo parâmetro `0`?

---

**7.** Sabendo que as variáveis, abaixo, surgiram numa listagem do utilitário `env`, faça o que for pedido.

Saída do `env`:

```
...
bicho=zebra
nome=Luis Carlos
...
```

**a.** Com o *builtin* `export`, escreva uma linha de comando que pode ter resultado neste efeito.

```
:~$ 
```

**b.** Como obter o mesmo efeito com o *builtin* `declare`?

```
:~$ 
```

**c.** Ainda com o `declare`, remova o atributo de exportação da variável `bicho`.

```
:~$
```

**d.** Faça o mesmo com a variável `nome`, só que com o comando `export`.

```
:~$
```

**e.** Com apenas um comando, elimine as variáveis `bicho` e `nome`.

```
:~$
```


---

**8.** Determine quais afirmações são **falsas** e explique por quê:

**a.** Nem todas as variáveis no ambiente de execução do shell estão no ambiente do processo.

**b.** O que determina se uma variável está no ambiente de execução do shell é o seu atributo de exportação.

**c.** O número de parâmetros no processo do shell é expandido com o parâmetro especial `#`.

**d.** Os elementos de um comando simples não têm nenhuma relação com o processo que vier a ser iniciado.

---

**9.** Altere a variável PATH de modo a incluir o diretório `$HOME/.local/bin`:

Resposta:

```
:~$
```

---

**10.** Sobre o exercício anterior, assinale as alternativas incorretas...

- [ ] `HOME` tem atributo de exportação.
- [ ] `HOME` expande o caminho do diretório `home` do usuário.
- [ ] O novo valor de `PATH` só passará a valer na sessão seguinte do shell.
- [ ] `PATH` não tem atributo de exportação.
- [ ] Variáveis exportadas não podem ser alteradas.

Justifique suas escolhas:

---


