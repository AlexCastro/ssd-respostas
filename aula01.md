# Aula 1: Salve, simpatia!

## Problema 1

Escreva um *script em shell* que imprima uma mensagem no terminal.

## Entendendo o problema…

Nosso primeiro problema traz algumas questões fundamentais:

- O que é um shell?
- O que é um script?
- O que significa imprimir?
- O que é um terminal?
- Como “imprimir” uma mensagem nele?

## O que é um shell?

O sistema operacional GNU/Linux é formado pelos seguintes componentes:

- O kernel (Linux);
- A biblioteca C padrão (glibc);
- O shell (Bash);
- Utilitários da base do sistema.

Obviamente, quando instalamos uma distribuição GNU/Linux em nossos
computadores, existirão outros programas à nossa disposição, mas esses quatro
são os componentes mínimos para que tenhamos um sistema operacional totalmente
funcional. Dentre eles, o usuário só tem acesso direto ao shell e, através
dele, aos utilitários da base do sistema.

> É assim porque o kernel e a biblioteca C padrão possuem atribuições mais
> relacionadas com o gerenciamento dos recursos de hardware (CPU, memória e
> dispositivos) e da execução de programas – daí nós dizermos que “ninguém usa
> o Linux”.

Quanto às suas atribuições, o shell do GNU/Linux pode ser entendido como um
interpretador de comandos programável e uma interface com o sistema
operacional, mas, acima de tudo, o shell é a interface padrão para a operação
do sistema: é através dessa interface que nós podemos escrever comandos que
expressem o que queremos que o sistema execute.

É importante ter em mente que, embora seja um componente tão essencial em um
sistema GNU/Linux, o shell é, no fim das contas, um programa como todos os
outros que utilizamos no dia a dia – tanto que existem vários programas criados
para funcionar como o shell do sistema. Na verdade, os primeiros shells foram
criados nos anos 1960 como interfaces para o sistema operacional MULTICS (1965)
e, posteriormente, a partir de 1969, para o UNIX.

> O GNU/Linux é classificado como um sistema operacional UNIX like (parecido
> com o UNIX). Existem, também, os sistemas operacionais da família UNIX, como
> os \*BSD, o Solaris e o macOS: que são, essencialmente, sistemas UNIX de
> fato.

Voltando ao shell, vários programas foram criados para assumir suas funções,
entre eles:

| Shell                 | Executável | Descrição                                                                                                                                                                                                                    |
|-----------------------|------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Shell Thompson        | `sh`       | O shell original criado por Ken Thompson, o criador do sistema UNIX.                                                                                                                                                         |
| Bourne Shell          | `sh`       | Escrito por Stephen Bourne, foi o sucessor do shell Thompson na versão 7 do UNIX.                                                                                                                                            |
| Bourne Again Shell    | `bash`     | Escrito originalmente por Brian Fox (e atualmente mantido por Chet Ramey), o Bash foi lançado em 1989 como o shell padrão do sistema operacional GNU: papel que ocupa até hoje no GNU com kernel Linux.                      |
| Almquist Shell        | `ash`      | Escrito por Kenneth Almquist com o propósito de ser um shell leve, clone do Bourne Shell utilizado na versão do UNIX conhecida como System V.4, e acabou sendo utilizado como o shell padrão do BSD no início dos anos 1990. |
| Debian Almquist Shell | `dash`     | Em 1997, Herbert Xu portou o ash para a distribuição Debian GNU/Linux, mas foi só em 2002 que o porte foi definitivamente nomeado como `dash`.                                                                                 |

Existem outros shells por aí, mas o ponto que requer a nossa atenção no momento
é que podemos ter vários shells diferentes instalados no sistema, mas
precisamos escolher qual deles será utilizado numa situação muito particular:
a execução de comandos a partir de outros programas.

Por história e convenção, todos os programas que executam comandos do shell
(como lançadores de outros programas, por exemplo) invocam o arquivo binário
executável identificado com o nome `sh` no diretório `/bin` (caminho
`/bin/sh`). Em sistemas da família UNIX, `/bin/sh` costuma ser, de fato, um
binário executável (geralmente, uma implementação do `ash` ou do `dash` nomeada
como `sh`). Nas distribuições GNU/Linux, porém, o mais comum é que `/bin/sh` seja
uma *ligação simbólica* (um outro nome para o mesmo arquivo) para o shell que
efetivamente assumirá esse papel. No Debian, por exemplo, essa ligação é feita
com o shell `dash`, mas não é incomum encontrar o Bash ligado ao `/bin/sh`, o que
faz com que ele se comporte de forma mais estritamente compatível com as
especificações de um shell UNIX: as normas POSIX.

Quando utilizado dessa forma, ou seja, invocado por outros programas, não há
como o usuário interagir com o shell: é por isso que o shell identificado por
`/bin/sh` é chamado de ***shell não interativo padrão*** ou ***shell não interativo POSIX***,
numa referência a um dos dois modos que todo shell POSIX pode receber os
comandos:

- **Modo interativo:** nós digitamos um comando, teclamos `<Enter>`, aguardamos
  o fim de seu processamento e decidimos o que fazer em seguida.
- **Modo não interativo:** os comandos são previamente escritos em um arquivo
  ou são passados para o executável do shell como argumentos ou fluxos de
  dados.

A escolha do shell que será utilizado por padrão no modo interativo pode ser
mais pessoal: cada um escolhe o que lhe parecer mais confortável. Porém, no
sistema GNU/Linux padrão, o shell inicialmente definido para uso interativo é
sempre o Bash – se não for o Bash, o que pode acontecer, não será um sistema
GNU padrão com kernel Linux.

> Isso não é importante só para definir o que é um sistema GNU, mas para
> anteciparmos aquilo que será encontrado em servidores e instalações do
> GNU/Linux com que teremos que lidar profissionalmente.

Já o uso não interativo do shell que mais nos interessa diz respeito aos
scripts, mas…

## O que são scripts?

Scripts são arquivos de texto contendo uma ou mais linhas dos mesmos comandos
que, se estivéssemos trabalhando interativamente com o shell, seriam digitadas
e interpretadas uma por vez. Só essa funcionalidade do shell já seria
suficiente para trazer um mundo de possibilidades: como, por exemplo, executar
procedimentos complexos com um único comando e, ao mesmo tempo, ter esses
mesmos procedimentos documentados em arquivos de texto. Contudo, o shell
possibilita voos ainda mais criativos quando implementa todos os elementos de
uma linguagem de programação procedural, estruturada de alto nível!

Para sermos ainda mais completos, a linguagem do shell é classificada como:

- **Interpretada:** o código é executado por um interpretador ainda na sua
  forma de texto, em vez de ser transformado em um arquivo binário e
  transferido para a memória quando invocado.
- **Imperativa:** nós expressamos o que queremos que seja feito através de
  comandos (*shell, faça isso, faça aquilo, etc…*).
- **Procedural:** o código do programa expressa um ou mais procedimentos.
- **Estruturada:** o fluxo de execução do código é controlado por
  sub-rotinas e estruturas de decisão e repetição.
- **De alto nível:** o código é escrito com elementos sintáticos e símbolos
  familiares para um leitor humano.

Não há limites para o que pode ser feito com scripts: basicamente, tudo que
pode ser feito no modo interativo pode ser transformado em um script, mas isso
não significa que tudo será possível apenas com as funcionalidades do próprio
shell! Embora seja possível solucionar alguns problemas usando apenas o que o
shell oferece, as suas funcionalidades de programação foram pensadas para que
nós pudéssemos determinar como, quando, sob quais condições e com que dados os
nossos comandos devem ser executados: qualquer comando, até os que incluem a
execução de outros programas!

Isso nos leva a outro aspecto fundamental do modo interativo de operação do
shell: o fato dele poder receber comandos como ***argumentos*** e ***fluxos de dados***.
Neste ponto, o shell funciona como qualquer outro utilitário da base do sistema
e maioria dos programas que implementam a chamada *“interface de linha de
comando”* (CLI), que tem suas origens no desenvolvimento de utilitários para o
UNIX e está fortemente ligado ao enunciado mais conhecido da ***filosofia UNIX*** –
aquele atribuído a Doug McIlroy:

- *Escreva programas que façam apenas uma coisa, mas que a façam bem-feita.*
- *Escreva programas que trabalhem juntos.*
- *Escreva programas que manipulem fluxos de texto, pois esta é uma interface universal.*

Vale a pena destacar dois aspectos muito importantes dessa forma de enunciar a
filosofia UNIX. Veja que os dois primeiros preceitos sugerem uma abordagem
***modular*** para a solução de problemas computacionais – ou seja, um programa não
precisa ser capaz de resolver tudo sozinho: parte do processamento que levará à
solução pode (e deve) ser delegado a outros programas mais especializados.
Enquanto isso, o terceiro preceito estabelece o meio pelo qual os programas
devem trabalhar juntos: trocando dados entre si na forma de cadeias de
caracteres (*fluxos de texto*, no fim das contas).

Todo o ecossistema UNIX (e GNU), formado pelo shell e os utilitários da base do
sistema, foi desenvolvido para funcionar segundo esses princípios, o que
atribui ao shell mais uma definição: além de um interpretador de comandos, uma
interface com o usuário, uma interface com o sistema e uma linguagem de
programação, o shell também é uma ***plataforma***!

> Em computação, uma plataforma é qualquer ambiente projetado para funcionar e
> ser ampliado de forma consistente com um conjunto de especificações
> características de seus componentes. No caso da plataforma shell, são a
> modularidade e os fluxos de texto da interface CLI que determinam a
> consistência do ambiente.

Por falar em fluxos de texto, este é um bom momento para entendermos…

## O que significa “imprimir”?

Embora a operação no modo interativo nos induza a pensar que estamos atuando
diretamente sobre o shell, a verdade é que nós interagimos apenas com o
terminal: é ele quem recebe os comandos que digitamos e exibe os textos que
recebemos como resposta. Essa confusão acontece porque, hoje, os terminais são
programas exibidos em monitores de vídeo, mas nem sempre foi assim. Os
primeiros terminais eram máquinas eletromecânicas compostas por um teclado e
uma impressora, chamadas de *teleimpressoras* (ou *teletipos*).

As teleimpressoras eram utilizadas, originalmente, no serviço de telegrafia e
foram modificadas, no fim dos anos 1950, para uso experimental como interfaces
de computadores. Foi somente no começo dos anos 1960 que a *Teletype Corporation*
produziu as primeiras teleimpressoras dedicadas ao uso na computação, entre
elas, a *Model 33 ASR*: já completamente adaptada para uso como terminal. Da
abreviação da marca *Teletype*, veio o TTY que nós utilizamos, até hoje, para
designar terminais.

Esse pequeno vislumbre da história é importante para entendermos a origem de
alguns termos e conceitos fundamentais para o domínio do shell e da criação de
scripts. Por exemplo, vejamos novamente o terceiro preceito da filosofia
UNIX:

> Escreva programas que manipulem fluxos de texto, pois esta é uma interface
> universal.

Terminais e computadores eram equipamentos fisicamente separados que trocavam
dados entre si através de cabos. O que fluía por esses cabos eram sinais
digitais (zeros e uns) que, a cada certa quantidade de pulsos elétricos,
representavam um caractere que iria compor um texto, controlar alguma função do
terminal ou sinalizar algum evento para o software em execução no computador.

Naturalmente, a associação feita entre cada conjunto de pulsos digitais e seu
significado deveria seguir algum tipo de convenção, e foi dessa necessidade que
surgiram os caracteres da ***tabela ASCII*** – uma especificação que atribui um
caractere para cada um dos 128 números possíveis de serem escritos com a
combinação de 7 bits.

Sabendo disso, é inevitável que o terceiro preceito da filosofia UNIX ganhe
outra dimensão, visto que os *“fluxos de texto”* (ou *“cadeias de caracteres”*)
faziam parte da realidade tangível da computação no fim dos anos 1960, quando o
UNIX nasceu. Também não havia nada de metafórico em algumas expressões
utilizadas corriqueiramente na época, como:

- **Digitar comandos no terminal:** o teclado era parte do terminal e as
  pessoas tinham que se deslocar, literalmente, até ele para digitar seus
  comandos.
- **Imprimir no terminal:** todos os textos resultantes de comandos eram,
  literalmente, impressos no papel de formulário contínuo dos terminais.

Por esse mesmo motivo, não era possível confundir o terminal com o shell, como
tivemos que advertir no início deste tópico: o terminal era, claramente, o
equipamento onde os comandos eram digitados e os resultados eram impressos,
enquanto o shell era o programa em execução no computador que mantinha uma
conexão constante com ele.

Por falar em conexão, desde os primórdios, os dados transitavam entre o shell e
o terminal por três vias – os ***“fluxos de dados padrão”***:

- **Entrada padrão:** a ligação entre o teclado do terminal com o computador.
- **Saída padrão:** a ligação entre a impressora do terminal e o computador.
- **Saída padrão de erros:** também é uma ligação com a impressora do
  terminal, mas trata-se de uma via utilizada pelo computador apenas para imprimir
  mensagens de erro.

> O termo “padrão”, associado a esses três fluxos de dados, refere-se ao fato
> de que, “por padrão”, eles são as vias de origem e destino dos caracteres que
> transitam entre os programas e o terminal.

### O que é um terminal hoje em dia?

Aliás, está na hora de fazermos um pequeno ajuste de terminologia. Até então,
eu tenho utilizado a palavra “computador” para me referir à outra ponta da
comunicação com o terminal. Apesar de estar tecnicamente correto (as vias
físicas realmente se ligam ao terminal e ao computador), quem está, de fato, no
controle do hardware é o kernel do sistema operacional.

> Os sistemas operacionais foram criados justamente para que os programas não
> precisassem mais lidar diretamente com as complexidades do controle do
> hardware. No UNIX, isso passou a ser atribuição de um kernel a que todos os
> programas pudessem requisitar acesso, de forma padronizada, aos recursos da
> máquina.

Em computação, sempre que falamos de “remover complexidades”, nós utilizamos o
termo *“abstrair”*. Portanto, uma das funções do kernel do sistema operacional é
*abstrair o hardware*. Isso é feito de várias formas, mas o kernel Linux, assim
como o kernel do UNIX, cumpre este papel construindo uma representação completa
do hardware na forma de arquivos especiais chamados de ***dispositivos***.

> É daí que vem a famosa afirmação de que, “no UNIX”, assim como no Linux,
> “tudo é arquivo”.

Sendo assim, até as vias físicas por onde os dados fluem por padrão são
abstraídas na forma de dispositivos pelo kernel, o que podemos confirmar
listando seus nomes no diretório `/dev` (de *devices*, ou “dispositivos”, em
português):

```
:~$ ls -l /dev/std*
lrwxrwxrwx 1 root root 15 mar 31 01:54 /dev/stderr -> /proc/self/fd/2
lrwxrwxrwx 1 root root 15 mar 31 01:54 /dev/stdin -> /proc/self/fd/0
lrwxrwxrwx 1 root root 15 mar 31 01:54 /dev/stdout -> /proc/self/fd/1
```

Aqui…

- `/dev/stdin`: é a entrada padrão;
- `/dev/stdout`: é a saída padrão;
- `/dev/stderr`: é a saída padrão de erros.

Mas, apesar dos nomes dos arquivos baterem com o que esperávamos, algo parece
não estar certo: eles não são arquivos de dispositivos, e sim ligações
simbólicas com outros arquivos em `/proc/self/fd`!

Para entender o que está acontecendo, nós precisamos considerar dois outros
fatos: primeiro, o fato de que o mesmo computador pode ser operado através de
vários terminais simultaneamente (o UNIX e o Linux são sistemas multiusuários);
segundo, o mesmo usuário pode executar vários programas ao mesmo tempo, todos,
em princípio, com acesso ao terminal (o UNIX e o Linux são sistemas
multitarefas).

Novamente, é o kernel que tem a atribuição de administrar a execução de
programas e o acesso que eles terão aos recursos do hardware, o que é feito por
meio de ***processos***.

> Processos são estruturas de dados (na memória) associadas a cada programa em
> execução no sistema.

Nós voltaremos a falar sobre processos, mas, por enquanto, só precisamos saber
que todo processo, ao ser iniciado, recebe acesso aos três fluxos de dados
padrão através de ligações simbólicas com um dispositivo de terminal, chamadas
de ***descritores de arquivos***, que podemos conferir listando, desta vez, o que há
no diretório `/proc/self/fd`, apontado na listagem anterior:

```
:~$ ls -l /proc/self/fd
total 0
lr-x------ 1 blau blau 64 abr 13 02:46 3 -> /proc/1457891/fd
lrwx------ 1 blau blau 64 abr 13 02:46 0 -> /dev/pts/4
lrwx------ 1 blau blau 64 abr 13 02:46 1 -> /dev/pts/4
lrwx------ 1 blau blau 64 abr 13 02:46 2 -> /dev/pts/4
```

> O nome “descritor de arquivo” (FD) diz repeito ao fato de que, sim,
> dispositivos são arquivos, mas também que essas ligações com eles podem ser
> mudadas para outros arquivos, como veremos em breve.

O diretório `/proc/self` (de *“si mesmo”*) é um *“link mágico”* para o diretório do
processo que o está acessando: no caso, o processo do programa `ls` que, no
exemplo, é identificado pelo número `1457891`. Repare também que, no exemplo, as
ligações simbólicas de número `0`, `1` e `2` estão apontando para o arquivo
`/dev/pts/4`, onde `pts` é parte de um dos dois tipos de terminais que temos
emulados na forma de software: os “pseudo TTY” (PTY), utilizados, por exemplo,
para a criação de terminais para ambientes gráficos.

Cada PTY é composto por dois dispositivos:

- **PTM** (*pseudo terminal principal*): mais relacionado com a lógica interna
  do programa do terminal.
- **PTS** (*pseudo terminal secundário*): dispositivo que atua diretamente com
  os processos que acessam o terminal.

Portanto, cruzando as informações obtidas nas duas listagens de diretórios
feitas anteriormente, nós podemos dizer que:

- A entrada padrão (`/dev/stdin`) está associada ao descritor de arquivos `0`;
- A saída padrão (`/dev/stdout`) está associada ao descritor de arquivos `1`;
- E a saída padrão de erros (`/dev/stderr`) está associada ao descritor de arquivos `2`.

Isso tudo é muito importante, e ainda há muito há ser dito sobre terminais e
processos, mas está na hora de avançarmos um pouco mais na solução do nosso
problema!

## Como imprimir uma mensagem no terminal?

Depois de tudo que vimos até aqui, já sabemos que…

- *“Imprimir”* significa exibir uma cadeia de caracteres no terminal.
- *“Exibir”* significa enviar caracteres para a *“saída padrão”* do terminal.
- Se a saída padrão é um arquivo, *“imprimir”* é o mesmo que *“escrever na saída padrão”*.

Agora, nós só precisamos descobrir o que temos à disposição para escrever
mensagens na saída padrão – em outras palavras, precisamos descobrir um comando
interno do shell, ou um programa, que, ao ser invocado, expresse a nossa
vontade de escrever uma mensagem no terminal. Contudo, mesmo que encontremos
algo no shell ou no sistema que faça o que queremos, nós ainda precisamos
entender o que são comandos e como escrevê-los!

Por isso, vamos segurar um pouco a pressa para dar uma espiada na… 

### Anatomia da linha de um comando

Um comando é a expressão, escrita de uma forma que o shell seja capaz de
interpretar, daquilo que nós queremos que o sistema faça. Isso significa que o
shell foi projetado para receber e interpretar a nossa digitação segundo uma
série de convenções, e nós temos que respeitar essas convenções se quisermos
que os nossos comandos sejam corretamente interpretados e executados.

As formas mais simples de comandos são compostas de, pelo menos, um dos três
elementos abaixo e na seguinte ordem:

```
[EXPORTAÇÕES] [INVOCAÇÃO [ARGUMENTOS]] [REDIRECIONAMENTOS]
```

Onde:

- **Exportações:** são definições de variáveis que estarão disponíveis apenas
  para aquilo que estiver sendo invocado.
- **Invocação:** pode ser uma *função* (um conjunto de comandos definidos na
  memória e identificados por um nome), um *comando interno* do shell ou qualquer
  *programa* disponível no sistema (também chamado de *“comando externo”*).
- **Argumentos:** lista de palavras que representam os dados adicionais e opções
  que fazem parte da sintaxe daquilo que estiver sendo invocado.
- **Redirecionamentos:** mecanismo do shell que estabelece ligações entre os
  fluxos de dados atribuídos ao processo do que vier a ser executado e um
  arquivo.

Vejamos alguns exemplos…

### Listagem dos comandos internos do Bash

Um ***comando interno***, ou ***“builtin”*** é uma funcionalidade interna do shell que pode
ser invocada na linha de um comando. O Bash possui (atualmente) 61 comandos
internos que podem ser listados, por exemplo, com o comando abaixo:

```
:~$ compgen -b
```

Nessa linha, observe que nós temos a invocação do *builtin* `compgen` seguida do
argumento `-b`. Se quisermos saber para que serve o `compgen`, bem como o efeito do
argumento `-b`, nós podemos recorrer ao comando interno que imprime a ajuda de
comandos internos do Bash: o `help`…

```
:~$ help compgen
```

Desta vez, `help` é a invocação e `compgen` é o argumento passado para ela: ou
seja, é uma palavra que será utilizada pelo comando `help` como uma informação
adicional (no caso, o nome do *builtin* sobre o qual exibir a ajuda).

Executando o comando, nós encontraremos a seguinte descrição no começo do texto
impresso:

> Exibe possibilidades de completação dependendo das opções.

Isso quer dizer que o *builtin* `compgen` gera palavras que o Bash poderá utilizar
para sugerir complementações quando teclarmos `TAB` na digitação de um comando –
que palavras serão geradas, dependerá da opção que passarmos para o compgen
como argumento.

Infelizmente, o `help` não informa as opções do compgen (elas podem ser
encontradas no manual do Bash: `man bash`), mas nós não precisamos ver isso
agora, porque eu já pesquisei e trouxe os dois argumentos que serão utilizados
nos nossos experimentos:

- Opção `-b`: gera os nomes dos comandos internos do Bash (*builtins*).
- Opção `-k`: gera os nomes das palavras reservadas do Bash.

Por falar em argumentos, o `help` também pode receber algumas opções, como, por
exemplo, a opção `-d`, que exibe uma descrição breve do tópico buscado em vez de
uma texto mais completo:

```
:~$ help -d help
help - Display information about builtin commands.
```

> Parece não haver nenhuma descrição curta traduzida para o português e, mesmo
> que essa aqui estivesse, ela traz um pequeno erro: o `help` não exibe apenas
> informações sobre builtins! Com ele, nós também encontramos ajuda sobre
> “palavras reservadas”, que não são comandos internos. 

Ou ainda…

```
:~$ help -d help compgen
help - Display information about builtin commands.
compgen - Display possible completions depending on the options.
```

O mais interessante do `help`, porém, é que os argumentos referentes aos tópicos
não precisam ser os nomes completos dos comandos buscados: eles podem ser
escritos parcialmente, desde que correspondam ao início dos nomes buscados.

Por exemplo:

```
:~$ help -d e
echo - Write arguments to the standard output.
enable - Enable and disable shell builtins.
eval - Execute arguments as a shell command.
exec - Replace the shell with the given command.
exit - Exit the shell.
export - Set export attribute for shell variables.
```

No exemplo, o padrão descreve qualquer nome iniciado com o caractere “e”, o que
resultou nos tópicos listados na saída. Portanto, se quiséssemos a descrição
breve de todos os tópicos cobertos pelo `help`, nós poderíamos fazer assim:

```
:~$ help -d ''
```

Para o shell, uma *“string vazia”*, escrita abrindo e fechando aspas sem nada
entre elas, também representa um caractere (é um *“caractere nulo”*), como
veremos em muitos outros exemplos no futuro. O ponto principal aqui, é que o
`help` recebeu dois argumentos: o `-d` e o caractere nulo, e isso fará diferença no
resultado da sua execução – sem o caractere nulo, o `help` se comportará como se
tivesse sido invocado sem argumentos, apesar da opção `-d`.

> **Importante!** Toda vez que escrevemos caracteres entre aspas, sejam simples
> ou duplas, nós estamos delimitando uma “palavra” e, por conseguinte, um
> argumento.

### Escrevendo a saída de comandos em arquivos

O *redirecionamento*, que também pode figurar em comandos simples, poderia ser
utilizado, por exemplo, para salvar a lista de descrições curtas produzida
pelo `help` em um arquivo, em vez de exibi-la no terminal.

Quanto à direção do fluxo de dados, os redirecionamentos podem ser:

- **Redirecionamentos de escrita:** quando um fluxo de saída (descritores de
  arquivos `1`, `2` ou ambos) é conectado a um arquivo.
- **Redirecionamento de leitura:** quando um fluxo de entrada (descritor de
  arquivo `0`) é conectado a um arquivo.

> Note que, embora todo processo receba uma ligação com os fluxos padrão ao ser
> iniciado, nada impede que outras vias de leitura e escrita de dados sejam
> criadas quando necessário.

A escrita, especificamente, ainda pode ser feita de duas formas: substituindo o
conteúdo anterior do arquivo, caso ele exista, ou adicionando novas linhas a
ele. Ambos podem ser chamados de “redirecionamentos de escrita”, mas o segundo
caso é mais conhecido como “redirecionamento de *append*” (em português,
“apêndice”). Seja como for, se o arquivo de destino não existir, ele será
criado antes da execução do comando.

Para acionar o mecanismo do redirecionamento, nós utilizamos os chamados
**operadores de redirecionamento**, que, para os fluxos de dados padrão, são os
seguintes:

| Operador      | Descrição                                                                                   |
|---------------|---------------------------------------------------------------------------------------------|
| `< ARQUIVO`   | Redireciona a entrada padrão para a leitura de um arquivo.                                  |
| `> ARQUIVO`   | Redireciona a saída padrão para a escrita em um arquivo.                                    |
| `2> ARQUIVO`  | Redireciona a saída padrão de erros (descritor de arquivos 2) para a escrita em um arquivo. |
| `>> ARQUIVO`  | Redireciona a saída padrão para append em um arquivo.                                       |
| `2>> ARQUIVO` | Redireciona a saída padrão de erros para *append* em um arquivo.                            |
| `&> ARQUIVO`  | Redireciona a saída padrão e a saída padrão de erros para escrita em um arquivo.            |
| `&>> ARQUIVO` | Redireciona a saída padrão e a saída padrão de erros para *append* em um arquivo.           |

Portanto, se quisermos escrever a saída do `help` em um arquivo (por exemplo,
`builtins.txt`), nós podemos executar o comando…

```
:~$ help -d '' > builtins.txt
```

### Exportações de variáveis

Não são muitos os comandos internos do Bash que recebem dados por exportação de
variáveis – um exemplo, é o *builtin* `read`:

```
:~$ help -d read
read - Read a line from the standard input and split it into fields.
```

> **Tradução:** lê uma linha da entrada padrão e a divide em campos.

A parte da leitura de uma linha da entrada padrão não deve ser difícil de
entender a essa altura, mas a divisão dessa linha em campos requer algumas
explicações adicionais. Por exemplo, digamos que o comando read seja utilizado
para ler o arquivo que acabamos de criar com a saída do help:

```
:~$ read < builtins.txt
:~$
```

> O redirecionamento de leitura substitui o teclado (entrada padrão original)
> pelo conteúdo de um arquivo.

Executando essa linha, nós não teremos nada impresso no terminal em resposta. É
assim, porque o `read` atribuirá a linha que for lida a uma ***variável***, cujo nome nós
poderíamos ter informado como argumento.

> No shell, variáveis são identificadores de dados armazenados na memória.

No exemplo, nós não passamos o nome de nenhuma variável, então o `read` utilizou
a sua própria variável padrão: `REPLY` – em outras palavras, o texto da linha
lida pelo `read` foi atribuído à variável `REPLY`.

Se quisermos utilizar a informação atribuída à variável `REPLY`, a única forma de
fazermos isso no shell é através da sua ***expansão***, que é o mecanismo pelo qual o
shell “troca” o nome da variável pelo seu valor ***antes*** do comando ser
executado.

> A expansão de valores de variáveis (um dos vários tipos de expansão, a
> “expansão de parâmetros”) ainda será estudada à exaustão. Ela é processada
> quando o shell encontra o cifrão (`$`) iniciando o nome de uma variável.

Sendo assim, nós poderíamos expandir a variável `REPLY` como argumento de outro
comando: um que (olha o *spoiler*) imprimisse argumentos na saída padrão – que é
exatamente o que o *builtin* `echo` faz:

```
:~$ help -p echo
echo - Write arguments to the standard output.
```

> **Tradução:** escreve argumentos na saída padrão.

Portanto…

```
:~$ read < builtins.txt
:~$ echo $REPLY
% - Resume job in foreground.
:~$
```

Se quisermos separar a linha lida em campos, tudo que temos que fazer é passar
para o `read` os nomes das variáveis que receberão esses campos:

```
:~$ read campo1 campo2 campo3 < builtins.txt
:~$ echo $campo1
%
:~$ echo $campo2
-
:~$ echo $campo3
Resume job in foreground.
```

Como podemos ver, as variáveis `campo1` e `campo2` receberam a primeira e a segunda
palavra da linha lida pelo read (respectivamente, as palavras `%` e `-`), enquanto
a variável `campo3` recebeu o restante da linha. Isso aconteceu por vários
motivos:

- Cada palavra de uma linha lida pelo `read` é delimitada, por padrão, pelos
  caracteres *espaço*, *tabulação* e *nova linha* (que, no caso, indica o fim da
  linha);
- Cada variável definida como argumento do `read` recebe apenas uma palavra;
- Se houver mais palavras do que variáveis, a última variável receberá o que
  sobrar na linha;
- Os três caracteres delimitadores de palavras são definidos em uma variável,
  globalmente acessível no shell, chamada `IFS` (de *“Internal Field Separator”*).

Disso tudo, porém, o mais importante é que os caracteres na variável `IFS` podem
ser alterados pelo usuário: ou seja, nós podemos definir quais serão os
caracteres utilizados para separar as palavras resultantes de expansões de
parâmetros (e outras do mesmo grupo). Mas os caracteres em `IFS` também
determinam o que o read utilizará para distribuir as palavras da linha de texto
lida nas variáveis que receber como argumentos.

Por exemplo:

```
:~$ IFS=e read campo1 campo2 campo3 < builtins.txt
:~$ echo $campo1
% - R
:~$ echo $campo2
sum
:~$ echo $campo3
job in foreground.
```

> Talvez este não seja o melhor exemplo da aplicação prática da separação de
> uma linha em campos, mas ele serve para demonstrar como o `read` faz essa
> separação de acordo com os caracteres `IFS`.

De qualquer forma, o que realmente requer a nossa atenção no momento é a
anatomia final da linha desse comando, que ficou assim:

```
IFS=e        read campo1 campo2 campo3  < builtins.txt
[Exportação] [Invocação e argumentos]   [Redirecionamento]
```

Especificamente sobre a exportação, repare que nós fizemos a atribuição de um
valor (`e`) à variável `IFS` na mesma linha do comando onde o `read` foi invocado:

```
IFS=e read campo1 campo2 campo3 < builtins.txt
```

Se isso tivesse sido feito em qualquer momento antes da linha do `read`, o valor
em `IFS` passaria a valer para todos os comandos subsequentes executados na mesma
sessão do shell.

```
:~$ echo "$IFS"


:~$ IFS=x
:~$ echo "$IFS"
x
```

Mas, como a atribuição foi feita na linha da invocação do `read`, ela afetou
apenas a sua execução, em vez de toda a sessão do shell, como podemos ver no
exemplo abaixo:

```
:~$ echo "$IFS"


:~$ IFS=e read campo1 campo2 campo3 < builtins.txt
:~$ echo "$IFS"


:~$
```

> As várias linhas em branco impressas pelo `echo` são resultantes da expansão
> dos caracteres originalmente definidos em `IFS`: espaço, tabulação e nova
> linha, mais a nova linha que o `echo` inclui, por padrão, na sua saída.

## Finalmente, “Salve, simpatia!”

Os nossos últimos experimentos acabaram fluindo naturalmente na direção de uma
possível solução para o problema de imprimir uma mensagem no terminal!

Nós já sabemos que o *builtin* `echo` faz exatamente o que queremos:

```
:~$ help -p echo
echo - Write arguments to the standard output.
```

> **Tradução:** escreve argumentos na saída padrão.

Portanto, basta definir o argumento que será passado para o `echo` – no caso, a
mensagem *“Salve, simpatia!”*, o que resulta na seguinte linha de comando:

```
:~$ echo 'Salve, simpatia!'
```

> Aqui, `'Salve, simpatia!'`, de aspas a aspas, é um único argumento.

O problema agora é descobrir como fazer isso em um script…

### Como criar um script

Antes de falarmos do processo de criação de scripts, é preciso enfatizar que
não há diferença entre o que fazemos na linha de comandos e o que escrevemos em
um script. Consequentemente, se o problema foi resolvido no modo interativo, a
mesma solução poderá ser escrita em um arquivo e utilizada na forma de um
script.

> Pode parecer uma afirmação óbvia, mas é muito fácil nos esquecermos de que o
> shell que interpreta os scripts é o mesmo que interpreta os comandos dados no
> terminal!

Seguindo em frente, o processo de criação de scripts geralmente envolve três
procedimentos gerais:

- Criar/editar o arquivo do script;
- Escrever a linha do interpretador de comandos (opcional);
- Tornar o arquivo do script executável (opcional).

A criação do arquivo pode ser feita de várias formas, sendo a mais comum aquela
que envolve o uso de um editor de textos “brutos” (*“raw text”*, em inglês), como
o Nano ou o Vim, no terminal, ou qualquer editor de código que você utilize no
ambiente gráfico.

> Aqui, nós não nos preocuparemos com a forma escolhida para criar e editar os
> arquivos dos scripts – utilize o editor da sua preferência!

### A linha do interpretador de comandos (hashbang)

Sobre a linha do interpretador de comandos, também chamada de *hashbang*, termo
que se refere aos caracteres cerquilha (`#`) e ponto de exclamação (`!`) em inglês,
é um recurso do sistema operacional para determinar qual programa será
executado para interpretar (ou apenas abrir) o conteúdo de um arquivo de
texto que tenha permissão de execução.

Nós já falaremos sobre permissões, mas os conceitos por detrás da linha do
interpretador de comandos exigem uma atenção um pouco mais imediata, pois dizem
respeito àquilo que o shell faz depois de interpretar os nossos comandos: que é
providenciar, efetivamente, a sua execução. Aliás, vale mencionar que o shell
só executa os seus próprios comandos internos, ou seja, qualquer outra coisa
deverá ser executada pelo sistema operacional.

Sendo assim, antes de prosseguirmos, nós precisamos entender…

### O que significa “executar”

Resumidamente, ***executar*** significa transferir o conteúdo do arquivo
binário de um programa para a memória do computador e indicar para a CPU que
ali estão as instruções que ela deverá processar. Este é um resumo superficial,
mas é suficiente para tirarmos algumas conclusões importantes:

- Somente os programas compilados em arquivos binários são executados;
- A CPU lê as instruções que estiverem na memória, não diretamente do arquivo
  do programa;
- Se CPU e memória são componentes de hardware, quem gerencia o seu uso é o
  kernel do sistema operacional, e nós já sabemos que o kernel gerencia a
  execução de programas através de ***processos***.

Portanto, se o shell tiver que providenciar a execução de um programa, ele terá
que solicitar ao sistema a criação de um processo, o que envolve a chamada de
duas funções da biblioteca C padrão: as chamadas de sistema da família `fork` e
`exec` (mais precisamente `clone` e `execve`).

> “Chamadas de sistema” é como nos referimos a funções especiais do kernel que
> são invocadas a partir de definições na biblioteca C padrão.

A chamada de sistema `fork` (que resulta, efetivamente, na chamada de sistema
`clone`, no GNU/Linux) inicia um novo processo como uma cópia fiel do processo
que realizou a chamada (um *clone*), diferindo apenas no seu número de
identificação (o seu PID). Depois da clonagem do processo, o arquivo que deverá
ser carregado na memória é passado como argumento da chamada de sistema `exec`.
Se o arquivo for encontrado, o conteúdo do processo clonado será substituído
pelo conteúdo relativo ao programa que será executado.

Apesar de envolver conceitos e mecanismos complexos, essa visão geral deve ser
simples o suficiente para levantar uma dúvida perfeitamente legítima: *se os
nossos scripts não são arquivos binários, como eles poderão ser executados?*

A resposta está exatamente naquilo que fizemos questão de relembrar:

> O shell que interpreta os scripts é o mesmo que interpreta os comandos dados
> no terminal!

Portanto, está claro que o programa a ser executado é o shell: o arquivo do
script contém apenas os comandos que deverão ser interpretados.

Mas…

### O que isso tem a ver com a hashbang?

Se o shell é o programa a ser executado (não interativamente), os comandos
deverão ser passados para ele de um dos modos abaixo:

- Passando uma string como valor da opção `-c`;
- Pela leitura da entrada padrão, ou…
- Passando o nome de um arquivo como argumento.

Vejamos como funcionam esses métodos, a começar pela opção `-c`:

```
:~$ bash -c 'echo Salve, simpatia!'
Salve, simpatia!
```

A sintaxe é:

```
bash -c COMANDOS
```

Onde `COMANDOS` é um único argumento correspondendo aos comandos que deverão ser
executados. Este método é muito utilizado em lançadores de programas, mas
utilizando o shell definido no caminho `/bin/sh`:

```
sh -c COMANDO
```

O shell também pode receber comandos na forma de fluxos de dados:

```
:~$ echo 'echo banana; echo laranja' | bash
banana
laranja
```

Aqui, nós usamos o `echo` para enviar uma string para a saída padrão que, por sua
vez, estava encadeada por *pipe* com o `bash`. O mesmo poderia ser feito, por
exemplo, se os comandos estivessem em um arquivo.

Por exemplo, se o conteúdo do arquivo `teste.txt` fosse:

```
echo banana
echo laranja
```

Nós poderíamos executar seu conteúdo usando um redirecionamento de leitura:

```
:~$ bash < teste.txt 
banana
laranja
```

Mas, quando se trata de ler comandos de um arquivo, o shell é capaz de,
simplesmente, receber o nome desse arquivo como argumento.

Portanto, isso teria o mesmo efeito:

```
:~$ bash teste.txt
banana
laranja
```

> **Importante!** Repare que a terminação `.txt`, ou qualquer outra terminação,
> não faz qualquer diferença para o shell!

Contudo, existe uma quarta opção para fazer o shell executar o conteúdo de um
arquivo: tornando o arquivo executável e invocando o seu nome.

### Permissão de execução

A rigor, a ação padrão do shell quando invocamos o nome de qualquer arquivo na
linha de comandos é executar o seu conteúdo. Neste sentido, podemos dizer que
todos os arquivos são, ao menos potencialmente, executáveis. O que vai
determinar se o shell tentará executar o conteúdo de um arquivo invocado no
comando são as ***permissões do usuário***.

Observe o que acontece quando tentamos executar o arquivo `teste.txt`:

```
:~$ ./teste.txt
bash: ./teste.txt: Permissão negada
```

> **Importante!** O `./` antes do nome do arquivo é apenas a indicação do seu
> caminho e pode ser lido como “neste diretório”.

Veja que a mensagem de erro foi bastante clara quanto à causa do problema, o
que resta saber é: permissão *de quem* para *fazer o quê*?

Relativamente a cada arquivo, individualmente, em sistemas *UNIX like* (como o
GNU) todo usuário tem três permissões configuráveis: *leitura*, *escrita* e
*execução*, representadas, respectivamente, pelas letras `r`, `w` e `x`. 

> Se alguma dessas permissões não estiver configurada, ela será representada
> com um traço (`-`).

Isso pode ser observado com uma listagem detalhada do arquivo (`ls -l`):

```
:~$ ls -l teste.txt
-rw-rw-r-- 1 blau blau 25 abr 18 07:39 teste.txt
```

Repare que, logo no começo da linha, nós temos três repetições das três
permissões: `rw-`, `rw-` e `r--`, e nas três repetições a permissão de execução
(`x`) aparece com um traço (-). Isso quer dizer que eu não tenho permissão para
executar o arquivo `teste.txt`.

Mas, por que as permissões aparecem três vezes?

Acontece que sistemas *UNIX like* têm vários usuários. Então, as permissões de
acesso a arquivos são configuradas para o usuário que for dono do arquivo, para
o grupo de usuários do dono do arquivo e para usuários dos demais grupos:

```
 +--------- Dono do arquivo.
 |  +------ Grupo do dono do arquivo.
 |  |  +--- Outros grupos de usuários.
 |  |  |
 ↓  ↓  ↓
-rw-rw-r-- 1 blau blau 25 abr 18 07:39 teste.txt
             ↑    ↑
             |    |
             |    +--- Grupo do dono do arquivo
             +-------- Dono do arquivo.
```

### Dando permissão de execução

A configuração de permissões, embora seja um procedimento simples, envolve
conceitos que nos desviariam muito do nosso foco. Por isso, sem descartar a
importância de pesquisar mais sobre o assunto, vamos nos limitar a ver como
podemos alterar a permissão de execução de um arquivo:

```
:~$ chmod +x teste.txt
```

Onde:

- `chmod`: utilitário para manipular permissões de arquivos.
- `-x`: argumento que dá permissão de execução a todos os usuários.
- `teste.txt` (no exemplo): o arquivo que será afetado.

O resultado é esse:

```
:~$ ls -l teste.txt
-rwxrwxr-x 1 blau blau 25 abr 18 07:39 teste.txt
```

> É simples assim, mas tem um detalhe: nós só podemos alterar permissões de
> arquivos que nos pertencem! O único usuário que pode alterar permissões de
> qualquer arquivo é o root.

### Executando o conteúdo do arquivo

Tendo permissão de execução, nós já podemos testar a afirmação de que o shell
tentará executar o conteúdo de qualquer arquivo que for invocado na linha de
comandos:

```
:~$ ./teste.txt 
banana
laranja
```

Sucesso! Mas, o que aconteceu aqui?

Sendo a invocação de um arquivo, não de um comando interno, o shell fez uma
chamada de sistema `fork` (para iniciar um novo processo como um clone de si
mesmo) e uma chamada `exec`, que, por sua vez, verificou se o arquivo era um
binário de um formato executável válido (formato ELF, no Linux). Sendo um
arquivo-texto, o `exec` buscou pelos caracteres `#!` bem no começo do arquivo. Como
não encontrou, a chamada `exec` terminou com erro e a execução e o conteúdo do
arquivo foi interpretado no clone recém-criado do processo shell.

Isso pode ser comprovado observado uma *flag* que o utilitário `ps` (que exibe
informações sobre processos) gera para informar como o início do processo
aconteceu: se a *flag* for `0`, o processo foi inciado passando pelas chamadas `fork`
e `exec`; se for `1` ou `5`, o processo foi clonado mas a chamada `exec` não foi
concluída ou simplesmente não foi feita.

Para demonstrar, vamos recorrer a um novo conceito do shell: os ***parâmetros
especiais***, que são como variáveis, mas são identificados por símbolos gráficos
e são controlados apenas pelo próprio shell. Um desses parâmetros especiais é o
cifrão (`$`), que recebe o número do processo do próprio shell. Para expandir seu
valor, nós escrevemos um outro cifrão antes dele, resultando na construção `$$`.

Veja neste exemplo:

```
:~$ echo $$
1843763
```

Este é o número de identificação do processo do shell corrente (seu PID).

Sabendo disso, nós podemos executar o utilitário `ps` da seguinte forma:

```
ps -o f PID
```

Onde:

- `ps`: Utilitário que exibe uma tabela de informações sobre os processos em
  execução no sistema.
- `-o`: Opção que possibilita definir quais colunas (campos) da tabela serão
  exibidas.
- `f`: Valor passado para `-o` indicando que queremos exibir apenas a coluna
  das *flags*.
- `PID`: Número de identificação do processo investigado.

Portanto, vejamos o que acontece quando executamos o comando abaixo na sessão
interativa do shell:

```
:~$ ps -o f $$
F
0
```

Note que a coluna das *flags* informou o valor `0`: o processo do shell passou
pelas chamadas de sistema `fork` e `exec`. Mas, o que aconteceria se esse mesmo
comando fosse escrito no nosso arquivo?

Para testar, vamos alterar o conteúdo do arquivo `teste.txt` para que fique desta
forma:

```
ps -o f $$
```

Se nós o executarmos novamente, o resultado será…

```
:~$ ./teste.txt 
F
1
```

A *flag* agora é `1`, indicando que o processo do shell, iniciado para executar o
conteúdo do arquivo, passou por uma chamada de sistema `fork`, mas não pela
chamada de sistema `exec`.

### O que muda com a hashbang?

Se os caracteres `#` (*hash*) e `!` (*bang*) forem encontrados bem no começo do
arquivo, o sistema lerá o que aparece em seguida na mesma linha, pois este será
o programa que receberá o nome do próprio arquivo como argumento para
interpretar ou abrir seu conteúdo. Em termos mais simples, a presença de uma
*hashbang* tem o mesmo efeito de invocar o programa na linha de comandos usando o
nome do arquivo como argumento. Se esse programa for o Bash, por exemplo, será
o mesmo que invocar do Bash passando o nome do arquivo do nosso script como
argumento.

Portanto, se isso estiver na primeira linha de um arquivo-texto executável:

```
#!/bin/bash
```

O efeito será exatamente igual ao desta forma de invocação:

```
:~$ bash ARQUIVO
```

> Podemos concluir que, efetivamente, o papel de uma *hashbang* é apenas
> possibilitar a execução ou a abertura de arquivos de texto (com permissão de
> execução) apenas invocando seu nome.

Sendo assim, vamos incluir a linha do interpretador de comandos no arquivo `teste.txt`:

```
#!/bin/bash

ps -o f $$
```

E vamos executá-lo:

```
:~$ ./teste.txt 
F
0
```

Desta vez, nós encontramos o valor `0` na coluna da *flag*: o novo processo do
shell foi clonado e passou pela chamada de sistema `exec`.

Antes de seguirmos em frente, uma última nota: arquivos executáveis com
*hashbang* ainda poderiam ser invocados da forma abaixo:

```
:~$ bash teste.txt
```

Porque, para o shell, tudo que vem depois do caractere `#` é um comentário, ou
seja, toda a linha da *hashbang* seria ignorada.

### A hashbang é obrigatória?

Não, nós só estamos falando do que acontece quando a utilizamos (ou não) nos
nossos scripts. A decisão de utilizá-la dependerá de vários fatores, como:

- O shell utilizado para interpretar o conteúdo do arquivo é indiferente (o
  script só executa utilitários e programas externos ao shell, por exemplo)?
- O seu sistema só tem um shell e o script só será executado por você?
- Mesmo que tenha vários shells, você sabe que só poderá executar o script a
  partir de um shell específico?
- O seu script só será executado automaticamente e no início de um shell
  específico (carregado pelo `~/.bashrc`, por exemplo)?

Se a resposta for *“sim”* para todas essas perguntas, tudo bem, a *hashbang* é
dispensável! Caso contrário, ou se nem se tratar de um script em shell (pode
ser Perl, PHP, Python, AWK, etc), aí sim, a hashbang é obrigatória.

### O certo não seria usar o ‘env’ na hashbang?

No shell, só uma coisa é certa: ***tudo depende***! A (falsa) controvérsia
sobre o uso do `env` tem a ver com eventuais dúvidas sobre a localização do
arquivo binário do shell que o script requer para ser executado.

No GNU/Linux, por exemplo, o Bash sempre estará no diretório `/usr/bin`, mas,
desde quando essa localização foi adotada, o diretório originalmente utilizado
para binários do sistema, que era `/bin`, passou a ser uma ligação simbólica para
`/usr/bin`.

> Ligações simbólicas (ou *links*) funcionam como um segundo nome para o mesmo
> arquivo, ou seja, se eu escrevo `/bin/bash`, eu me refiro ao mesmo arquivo em
> `/usr/bin/bash`.

A localização do binário é importante porque o sistema, na chamada `exec`, não
tem como saber onde os arquivos estão, portanto, nós precisamos informar o
caminho completo até o binário na *hashbang*:

```
#!/bin/bash
```

Ou…

```
#!/usr/bin/bash
```

O problema (que rarissimamente é um problema de fato) é que nem todos os
sistemas operacionais *UNIX like* armazenam o binário do Bash no diretório
`/bin/bash` ou em `/usr/bin/bash`. Na prática, com raríssimas exceções, isso não
faz a menor diferença, porque os scripts são executados nos próprios sistemas
operacionais em que foram criados ou em sistemas iguais aos utilizados quando
foram criados. Logo, o mínimo que se espera de quem cria esses scripts é que a
pessoa conheça o próprio sistema e saiba onde está o binário necessário para
executá-los!

O utilitário `env` executa programas, cujos nomes recebe como argumento,
utilizando os dados de ambiente definidos pelo usuário. Quando passamos o nome
de um executável para o `env`, o que acontece é que ele mesmo se
encarregará de localizar o arquivo no sistema utilizando uma lista de caminhos
encontrada na variável `PATH`.

Então, supondo que o binário do `env` sempre estará no diretório `/usr/bin` (o que
realmente se espera que esteja em sistemas *UNIX like*), muitos defendem que esta
seria a forma *“correta”* de escrever uma *hashbang*, ou que seria, no mínimo, a
forma *“portável”* – o que também não faz muito sentido prático:

```
#!/usr/bin/env INTERPRETADOR
```

No caso de scripts interpretados pelo Bash:

```
#!/usr/bin/env bash
```

Mas, quando se quer portabilidade, utiliza-se o shell no caminho `/bin/sh`, que é
universal para sistemas *UNIX like*, para criar um script de instalação capaz de
cuidar de todas as dependências e suas localizações. Além disso, mirando apenas
no shell que nos interessa, o Bash, ele só será instalado por padrão no GNU, o
que faz dele uma dependência a ser testada em todos os outros sistemas – mas,
se existir uma dependência, o script, por definição, já não será mais
portável!

Na verdade, os primeiros casos de uso do `env` na *hashbang* de que se tem notícia remontam
meados dos anos 1990, quando algumas linguagens interpretadas começaram a ser
distribuídas e instaladas em diversas versões no mesmo sistema operacional. Com
tantas versões disponíveis, o `env` passou a ser utilizado para especificar a
versão que estivesse definida como padrão para o sistema: se o programador
quisesse utilizar outra versão, ou ele alteraria a variável `PATH`, ou escreveria
diretamente o caminho para a versão desejada na *hashbang*.

Isso se assemelha muito ao que acontece no macOS em relação ao Bash: até o
momento, o Bash é instalado por padrão, mas na sua última versão com
licenciamento compatível com o sistema (versão 3.2.x). Com um pouco de
paciência, é possível instalar a última versão, resultando em dois binários:
`/usr/bin/bash` (o antigo) e `/usr/local/bin/bash` (o mais atual, instalado pelo
usuário). O problema é que, por se tratar de uma customização do sistema, nem
sempre o caminho do Bash mais recente será configurado para ser o primeiro
encontrado na variável `PATH`, e o nosso script, que depende do `env` para ser
“portável”, acabará sendo executado com o Bash 3.2.x.

É por isso que eu sempre recomendo:

- Utilize o caminho para o Bash na sua *hashbang* (`/bin/bash`);
- Se a portabilidade for realmente importante, não escreva seu script em Bash:
  escreva scripts estritamente compatíveis com o shell em `/bin/sh` e utilize
  este caminho na *hashbang*;
- Se quiser distribuir scripts em Bash para qualquer outro sistema, escreva um
  instalador compatível com o shell em `/bin/sh` e ajuste tudo que for necessário.
- Sempre questione tudo que for apresentado como *“o jeito certo”*;
- Nunca troque a compreensão por regras tiradas da… Eh… “da cartola”!

## O script final

Depois de uma primeira etapa longa e cheia de conhecimentos novos, nós estamos
prontos para entregar, oficialmente, o primeiro script em Bash da nossa jornada
de aprendizado:

Arquivo `salve.sh`:

```
#!/bin/bash

echo 'Salve, simpatia!'
```

> A terminação `.sh` está no nome do arquivo apenas para que nós saibamos o que
> há em seu conteúdo.

Para torná-lo executável:

```
:~$ chmod +x salve.sh
```

Para executá-lo:

```
:~$ ./salve.sh
Salve, simpatia!
```


