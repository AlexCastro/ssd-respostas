# Exercícios da aula 1

**1. Analise as afirmações abaixo e determine quais delas são falsas:**

1. O shell só é capaz de executar seus comandos internos.

   **Resposta:** Falso
   
   **Justificativas:** Ele é a principal interface com a operação do sistema e pode executar outros programas

2. O shell é um interpretador de comandos programável.

   **Resposta:** Verdadeiro

   **Justificativas:** o shell do GNU/Linux pode ser entendido como um interpretador de comandos programável e uma interface com o sistema operacional, mas, acima de tudo, o shell é a interface padrão para a operação do sistema: é através dessa interface que nós podemos escrever comandos que expressem o que queremos que o sistema execute.

3. Sem a linha do interpretador de comandos, o script será executado pelo shell **não interativo** padrão. 

   **Resposta:** Verdadeiro

   **Justificativas:** Sendo um arquivo-texto, o exec buscou pelos caracteres #! bem no começo do arquivo. Como não encontrou, a chamada exec terminou com erro e a execução e o conteúdo do arquivo foi interpretado no clone recém-criado do processo shell

4. No modo **interativo**, o shell só recebe comandos pela entrada padrão.

   **Resposta:** Falso

   **Justificativas:** pode receber de qualquer fluxo de texto


5. O shell `sh` sempre será encontrado no diretório `/bin` no GNU/Linux.   

   **Resposta:** Verdadeiro

   **Justificativas:** o shell no caminho /bin/sh, que é universal para sistemas UNIX like

---

**2. Utilizando o `help` e o `man bash`, descubra quais são as opções do *builtin* `compgen` para imprimir listas de...**

Comandos internos do Bash:

```
:~$ compgen -b
```

Palavras reservadas do Bash:

```
:~$ compgen -k
```

Tópicos de ajuda do comando intermo `help`:

```
:~$ compgen --help
```

---

**3. Pesquise e descubra uma forma de exibir o nome do dispositivo de terminal em uso:**

```
:~$ tty
```

---

**4. Pesquise e elabore comandos que demonstrem as seguintes afirmações:**

*"Imprimir/exibir no terminal"* é o mesmo que escrever no arquivo dispositivo de terminal:

```
:~$   echo "teste" > /dev/pts/4
```

Quando recebe um comando como argumento da opção `-c`, o Bash é executado no modo **não interativo**:

```
:~$ bash -c 'echo $-'
```

Em um redirecionamento de leitura, o link simbólico com o dispositivo do terminal é substituído pela ligação com um arquivo:

```
:~$  bash < teste.sh
```

---

**5. Com base nas ajudas do *builtin* `help`, descreva as diferenças entre os comandos `echo` e `printf`:**

**Resposta:**  o **echo**: Escreve argumentos para a saída padrão. e o **printf**: formata, convertida e imprimi na saida padrão

---

**6. Com base nas diferenças encontradas, reescreva o script `salve.sh` ([da aposila](aula01.md#o-script-final)) usando `printf`:**

Arquivo `salve.sh`:

```
#!/bin/bash

printf '%s\n' 'Salve, simpatia!'

```

---

**7. Ainda com o script `salve.sh`, faça com que ele escreva a mensagem abaixo com *apenas uma* invocação do comando `echo`.**

```
:~$ ./salve.sh
Salve, simpatia!
Qual é a sua graça?
```
**Resposta:** 
```
#!/bin/bash

echo -e 'Salve, simpatia!\nQual é a sua graça?'
```

---

**8. Pensando na portabilidade, eu utilizei a seguinte *hashbang* no meu script:**

Arquivo do script:

```
#!/usr/bin/env sh
```

**Perguntas:**

- Essa *hashbang* é válida?

**Resposta (com justificativa):** sim, irá executar o sh em outro ambiente recebendo as variaveis de ambiente.

- Se for válida, ela é necessária para o meu propósito?

**Resposta (com justificativa):** Não, o sh é requisito nos S.O. unix-like, não precisando das variaveis PATH para descrobrir onde está o executavel sh.

- Se não for válida ou necessária, qual seria a forma correta?

```
#!/bin/sh
```

**Justifique:**  Nos S.O. unix-like sempre deve existir /bin/sh, seja em forma de um link ou o proprio executavel.

---

**9. Qual é o significado da extensão `.sh` no nome do arquivo `salve.sh`?**

**Resposta:**  Somente informativo, facilitar a identificação de que se trata de um script.

---

**10. O script `salve.sh` precisa de permissão de execução para ser executado?**

**Resposta:** Não
 
**Justifique:** Ele pode ser executado de outras formas como, por exemplo:

```
bash salve.sh
```

---

**11. Descreva a finalidade dos utilitários abaixo, que foram mencionados nos vídeos da aula 1:**

- `cat`: concatenate files and print on the standard output
- `chmod`: change file mode bits
- `env`: run a program in a modified environment
- `ls`: list directory contents
- `man`: uma interface para os manuais de referência do sistema
- `ps`: report a snapshot of the current processes.
- `pstree`: exibe uma árvore de processos
- `rm`: remove files or directories
- `tty`: print the file name of the terminal connected to standard input
- `w`: Mostra quem está conectado e o que está fazendo.
- `whoami`: print effective userid

---

**12. Pesquise e descubra como exibir descrições curtas de *programas*:**

**Resposta:**

```
:~$  man -k programa
```

---

**13. O que expandem os parâmetros especiais `-` e `$`?**

- `-`: Expands to the current option flags as specified upon invocation, by the set builtin command, or those set by the shell itself (such as the -i option).
- `$`: Expands to the process ID of the shell.  In a () subshell, it expands to the process ID of the current shell, not the subshell.

---

**14. Observe as duas formas de execução do script `teste.sh`, abaixo, e explique (justificando) o que for pedido.**

**Script `teste.sh`:**

```
echo As opções de início do Bash são: $-
```

**Execução 1:**

```
:~$ bash teste.sh
As opções de início do Bash são: hB
```

**Execução 2:**

```
:~$ source teste.sh
As opções de início do Bash são: himBHs
```

- Além da saída, qual é a principal diferença entre as duas formas de execução do script?
   
   *a execução 1 criou um novo processo do bash para executar o scritp, enquanto a 2 executou no processo/shell atual.*

- Por que a saída 1 não exibe o caractere `i` e a saída 2 exibe?

   *Na 1 uma nova sessão foi iniciada e sem o parametro -i. Já na 2 o shell já estava rodando no modo interativo*

- O fato do caractere `i` aparecer na segunda saída significa que os comandos foram executados interativamente?

   *Não, porque não teve uma tomada de decisão entre os comandos, simplesmente foi executado uma sequencia programada no scritp.*


---

**15. Utilizando `man bash` e `help set`, pesquise os significados dos caracteres `himBHs`, expandidos pelo parâmetro `-` no modo interativo:**

- `h`: Memoriza a localização de comandos à medida em que são procurados.
- `i`: If the -i option is present, the shell is interactive.
- `m`: Controle de trabalho está habilitado.
- `B`: o shell vai realizar expansão de chaves
- `H`: Habilita substituição de histórico estilo "!". Essa sinalização está habilitada por padrão quando  shell é interativa.
- `s`: If the -s option is present, or if no arguments remain after option processing, then commands are read from the standard input.  This option allows the positional parameters to be set when invoking an interactive shell or when reading input through a pipe.

---