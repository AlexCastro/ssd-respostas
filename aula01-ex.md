# Exercícios da aula 1

**1. Analise as afirmações abaixo e determine quais delas são falsas:**

1. O shell só é capaz de executar seus comandos internos.
2. O shell é um interpretador de comandos programável.
3. Sem a linha do interpretador de comandos, o script será executado pelo shell **não interativo** padrão.
4. No modo **interativo**, o shell só recebe comandos pela entrada padrão.
5. O shell `sh` sempre será encontrado no diretório `/bin` no GNU/Linux.

**Resposta:**

**Justificativas:**

---

**2. Utilizando o `help` e o `man bash`, descubra quais são as opções do *builtin* `compgen` para imprimir listas de...**

Comandos internos do Bash:

```
:~$ compgen 
```

Palavras reservadas do Bash:

```
:~$ compgen
```

Tópicos de ajuda do comando intermo `help`:

```
:~$ compgen
```

---

**3. Pesquise e descubra uma forma de exibir o nome do dispositivo de terminal em uso:**

```
:~$
```

---

**4. Pesquise e elabore comandos que demonstrem as seguintes afirmações:**

*"Imprimir/exibir no terminal"* é o mesmo que escrever no arquivo dispositivo de terminal:

```
:~$ 
```

Quando recebe um comando como argumento da opção `-c`, o Bash é executado no modo **não interativo**:

```
:~$
```

Em um redirecionamento de leitura, o link simbólico com o dispositivo do terminal é substituído pela ligação com um arquivo:

```
:~$ 
```

---

**5. Com base nas ajudas do *builtin* `help`, descreva as diferenças entre os comandos `echo` e `printf`:**

**Resposta:**

---

**6. Com base nas diferenças encontradas, reescreva o script `salve.sh` ([da aposila](aula01.md#o-script-final)) usando `printf`:**

Arquivo `salve.sh`:

```
#!/bin/bash


```

---

**7. Ainda com o script `salve.sh`, faça com que ele escreva a mensagem abaixo com *apenas uma* invocação do comando `echo`.**

```
:~$ ./salve.sh
Salve, simpatia!
Qual é a sua graça?
```

---

**8. Pensando na portabilidade, eu utilizei a seguinte *hashbang* no meu script:**

Arquivo do script:

```
#!/usr/bin/env sh
```

**Perguntas:**

- Essa *hashbang* é válida?

**Resposta (com justificativa):**

- Se for válida, ela é necessária para o meu propósito?

**Resposta (com justificativa):**

- Se não for válida ou necessária, qual seria a forma correta?

```

```

**Justifique:**

---

**9. Qual é o significado da extensão `.sh` no nome do arquivo `salve.sh`?**

**Resposta:**

---

**10. O script `salve.sh` precisa de permissão de execução para ser executado?**

**Resposta:**

**Justifique:**

---

**11. Descreva a finalidade dos utilitários abaixo, que foram mencionados nos vídeos da aula 1:**

- `cat`:
- `chmod`:
- `env`:
- `ls`:
- `man`:
- `ps`:
- `pstree`:
- `rm`:
- `tty`:
- `w`:
- `whoami`:

---

**12. Pesquise e descubra como exibir descrições curtas de *programas*:**

**Resposta:**

```
:~$ 
```

---

**13. O que expandem os parâmetros especiais `-` e `$`?**

- `-`:
- `$`:

---

**14. Observe as duas formas de execução do script `teste.sh`, abaixo, e explique (justificando) o que for pedido.**

**Script `teste.sh`:**

```
echo As opções de início do Bash são: $-
```

**Execução 1:**

```
:~$ bash teste.sh
As opções de início do Bash são: hB
```

**Execução 2:**

```
:~$ source teste.sh
As opções de início do Bash são: himBHs
```

- Além da saída, qual é a principal diferença entre as duas formas de execução do script?
- Por que a saída 1 não exibe o caractere `i` e a saída 2 exibe?
- O fato do caractere `i` aparecer na segunda saída significa que os comandos foram executados interativamente?

---

**15. Utilizando `man bash` e `help set`, pesquise os significados dos caracteres `himBHs`, expandidos pelo parâmetro `-` no modo interativo:**

- `h`:
- `i`:
- `m`:
- `B`:
- `H`:
- `s`:

---


